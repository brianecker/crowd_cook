# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations
import json
import multiprocessing
from datetime import datetime

import ingredient_handling
from recipe_handling import RecipeInsertionManager, Recipe
import recipe_suggestions.popular as popular
import solr.query


def test_insert(link):
    recipe_insertion_manager = RecipeInsertionManager()
    print "got recipe_id {0}".format(recipe_insertion_manager.insert_from_url(link))


popular_recipe_retriever = popular.PopularRecipeRetriever()


def index():
    links = [
        "http://www.epicurious.com/recipes/food/views/fresh-herb-falafel-56390104",
        "http://www.epicurious.com/recipes/food/views/sweet-potato-tempura-and-steak-sandwich-with-bok-choy-and-miso-mayo-51234910",
        "http://www.epicurious.com/recipes/food/views/strip-steak-with-roasted-acorn-squash-and-sprouted-lentils"
    ]
    for link in links:
        p = multiprocessing.Process(target=test_insert, args=(link,))
        p.start()

    num_items_each = 6
    return dict(popular=popular_recipe_retriever.get_popular_recipes(num_items_each),
                recent=_get_recently_added(num_items_each))


def search_or_crawl():
    search_request = request.vars["search_request"]
    recipe_insertion_manager = RecipeInsertionManager()
    if recipe_insertion_manager.has_retrieval_instance(search_request):
        if auth.is_logged_in():
            recipe_id = recipe_insertion_manager.insert_from_url(search_request)
            return response.json(dict(url=URL("recipe", args=[Recipe.get_recipe_url_from_id(recipe_id)])))
        else:
            return response.json(dict(error="You must be logged in to insert a recipe from a url"))
    return response.json(dict(url=URL("search", vars={"query": search_request})))


def _get_recently_added(n):
    return db(db.recipes.is_skeleton == "False").select(orderby=~db.recipes.created_on)[:n]


def search():
    print("Doing search")
    query = request.vars["query"]

    query_res = solr.query.recipes_query(query)
    results = list()
    if "response" in query_res and "docs" in query_res["response"]:
        for doc in query_res["response"]["docs"]:
            doc["url"] = URL("default", "recipe", args=[Recipe.get_recipe_url_from_id(doc["id"])])
            results.append(doc)
    return dict(result=results)


def add_recipe():
    if not auth.is_logged_in():
        redirect(URL("default", "user", args=["login"]))
    return dict()


@auth.requires_login()
@auth.requires_signature()
def add_recipe_endpoint():
    ractive_recipe = json.loads(request.vars["json_ractive"])
    recipe_url = RecipeInsertionManager.insert_from_ractive_dict(ractive_recipe)
    return response.json(dict(recipe_url=recipe_url))


def recipe():
    """
    Display page for a recipe
    Returns:

    """
    recipe_url = request.args(0)
    return dict(recipe=Recipe.load_from_db(Recipe.get_recipe_id_from_url(recipe_url)))


# probably don't need this
# def retrieve_recipe():
#     """
#     Endpoint for recipe retrieval
#     Returns: returns a dictionary representation of the recipe
#
#     """
#     recipe_url = request.args(0)
#     return response.json(dict(recipe=Recipe.load_from_db(Recipe.get_recipe_id_from_url(recipe_url))))


@auth.requires_login()
@auth.requires_signature()
def toggle_favorite():
    recipe_id = request.vars["recipe_id"]
    favorited = db((db.recipe_favorites.recipe_id == recipe_id) & (db.recipe_favorites.user_id == auth.user_id))
    if favorited.select().first() is None:
        db.recipe_favorites.insert(recipe_id=recipe_id, user_id=auth.user_id, favorited_on=datetime.now())
    else:
        favorited.delete()
    return response.json(dict())


def get_ingredient_suggestions():
    ingredient_id = request.vars["ingredient_id"]
    return response.json(ingredient_handling.get_ingredient_suggestions(ingredient_id, auth.user_id))


def add_ingredient_suggestion():
    # Check here instead of using decorators because we want to redirect via ajax
    if not auth.is_logged_in():
        return response.json(dict(redirect=URL("default", "user", args=["login"])))
    ingredient_id = int(request.vars["ingredient_id"])
    suggested_quantity = request.vars["suggested_quantity"]
    result = ingredient_handling.ingredient_suggestion.insert_suggestion(ingredient_id, suggested_quantity,
                                                                         auth.user_id)
    return response.json(result)


@auth.requires_login()
@auth.requires_signature()
def vote_ingredient_suggestion():
    ingredient_suggestion_id = request.vars["ingredient_suggestion_id"]
    vote_direction = request.vars["direction"]
    return response.json(
        ingredient_handling.ingredient_suggestion.vote(auth.user_id, ingredient_suggestion_id, vote_direction))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    # if request.args(0) == "profile":
    #     return dict(form=auth())

    result = None
    auth_form = auth()
    if auth.is_logged_in():
        _user = db(db.auth_user.id == auth.user_id).select().first()
        result = dict(form=auth_form,
                      first_name=_user.first_name,
                      last_name=_user.last_name,
                      email=_user.email,
                      spicy=_user.spicy,
                      sweet=_user.sweet,
                      bitter=_user.bitter,
                      salty=_user.salty,
                      sour=_user.sour,
                      )
    else:
        result = dict(form=auth_form,
                      first_name="",
                      last_name="",
                      email="",
                      spicy="",
                      sweet="",
                      bitter="",
                      salty="",
                      sour=""
                      )

    return result


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
