#########################################################################
## Define your tables below; for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

# auth_user_extra_fields = [
#     Field("spicy", "float"),
#     Field("salty", "float"),
#     Field("sweet", "float"),
#     Field("bitter", "float"),
#     Field("sour", "float")
# ]
from datetime import datetime

db.define_table("recipes",
                Field("recipe_title", "text"),
                Field("original_url", "text"),
                # Field("forked_from", db.recipes),
                # Can't make a foreign key to the same table for some reason
                Field("forked_from", "integer"),
                Field("author", "text"),
                Field("publisher", "text"),
                Field("publication_date", "text"),
                Field("is_skeleton", "boolean"),
                Field("url", "text"),
                Field("created_on", "datetime",
                      requires=IS_DATETIME(),
                      default=datetime.utcnow())
                )

db.define_table("ingredient_groups",
                Field("recipe_id", db.recipes),
                Field("group_title", "text"),
                Field("ingredient_group_index", "integer"))

db.define_table("ingredient_names",
                Field("ingredient_name", "text"),
                Field("created_on", "datetime",
                      requires=IS_DATETIME(),
                      default=datetime.utcnow())
                )

db.define_table("ingredients",
                Field("ingredient_group_id", db.ingredient_groups),
                Field("ingredient_name_id", db.ingredient_names),
                Field("amount", "text"),
                Field("units", "text")
                )

db.define_table("ingredient_links",
                Field("ingredient_id", db.ingredients),
                Field("linked_recipe_id", db.recipes),
                Field("link_start", "integer"),
                Field("link_end", "integer")
                )

db.define_table("ingredient_suggestions",
                Field("ingredient_id", db.ingredients),
                Field("suggested_quantity", "float"),
                Field("suggesting_user", db.auth_user),
                )

db.define_table("ingredient_suggestion_votes_up",
                Field("ingredient_suggestion_id", db.ingredient_suggestions),
                Field("voting_user", db.auth_user)
                )

db.define_table("ingredient_suggestion_votes_down",
                Field("ingredient_suggestion_id", db.ingredient_suggestions),
                Field("voting_user", db.auth_user)
                )

db.define_table("photos",
                Field("recipe_id", db.recipes),
                Field("original_photo", "boolean"),
                Field("votes", "integer"),
                Field("posted_by", db.auth_user),
                )

db.define_table("instruction_groups",
                Field("recipe_id", db.recipes),
                Field("instruction_group_index", "integer"),
                Field("instruction_group_title", "text")
                )

db.define_table("instructions",
                Field("instruction_group_id", db.instruction_groups),
                Field("instruction_index", "integer"),
                Field("instruction_text", "text"),
                )

db.define_table("recipe_favorites",
                Field("recipe_id", db.recipes),
                Field("user_id", db.auth_user),
                Field("favorited_on", "datetime"),
                requires=IS_DATETIME(),
                default=datetime.utcnow()
                )
