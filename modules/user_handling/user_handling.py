from gluon import current


def get_user(user_id):
    db = current.db
    return db(db.auth_user.id == user_id).select().first()