import re


class UndefinedUnitException(Exception):
    pass


def find_all_groups(pattern, string):
    """
    Finds all matches of pattern in string and converts them to a list of dictionary groups
    Args:
        pattern: regex pattern
        string: string to search

    Returns: list of dictionary groups
    """
    return [m.groupdict() for m in re.finditer(pattern, string)]


# Units list. SINGULAR ONLY!
UNITS = [
    "cup",
    "ounce",
    "teaspoon",
    "tablespoon",
    "liter",
    "litre",
    "gallon",
    "pint",
    "gram",
    "milliliter",
    "millilitre",
]

# A reverse lookup to transform plurals back to singular
UNITS_LOOKUP = dict()
for unit in UNITS:
    UNITS_LOOKUP[unit] = unit
    UNITS_LOOKUP[unit + "s"] = unit

# Make a units regex that includes plurals. At some point we might need to rewrite this code to take into account
# non-standard plural formations.
units_str = "|".join(["({unit}s?)".format(unit=unit) for unit in UNITS])
re_quantity = re.compile(r"(?P<amount>[0-9]+(/[0-9]+)?) (?P<units>" + units_str + r")?")


def normalize_units(unit):
    if unit not in UNITS_LOOKUP:
        raise UndefinedUnitException("'{unit}' is not a valid unit.".format(unit=unit))

    return UNITS_LOOKUP[unit]

