import hashlib
import multiprocessing
import re

import solr.query
from gluon import current
from invalid_recipe_url_error import InvalidRecipeUrlError
from retrieval_class_listing import retrieval_class_listing


class NoKnownRecipeRetrieverException(Exception):
    pass


class RecipeInsertionManager:
    queued_urls = dict()
    url_queuing_lock = multiprocessing.Lock()

    def __init__(self):
        # The number of recipes added to RecipeInsertionManager.insertion_queue by this instance
        self.num_queued_recipes = 0
        self.linked_recipes = list()

    @staticmethod
    def has_retrieval_instance(url):
        """
        Args:
            url: recipe url

        Returns: True if we can find a recipe retriever

        """
        recipe = None
        for retrieval_class in retrieval_class_listing:
            try:
                recipe = retrieval_class(url)
                break
            except InvalidRecipeUrlError:
                recipe = None

        return recipe is not None

    @staticmethod
    def _get_recipe(url):
        """

        Returns: A instance of RetrievedRecipe or None if none can be found that match the url

        """
        recipe = None
        for retrieval_class in retrieval_class_listing:
            try:
                # Try to setup a recipe retriever until success
                recipe = retrieval_class(url)
                break
            except InvalidRecipeUrlError:
                # Continue to next recipe retriever
                recipe = None

        if recipe is None:
            raise NoKnownRecipeRetrieverException(url)

        return recipe

    def queue_url(self, url, add_to_insertion_queue=False):
        """
        Queues a url in a thread-safe way.
        Args:
            url: The url to queue
            add_to_insertion_queue: True if this link doesn't have an owner and needs to be cleaned up at some point
            by the insertion_queue

        Returns: True if the url is not in the database or the queue, false if it is in the database or the queue
        """
        recipe = current.db(current.db.recipes.original_url == url).select().first()
        queued = False
        if recipe is None:
            RecipeInsertionManager.url_queuing_lock.acquire()

            if url not in RecipeInsertionManager.queued_urls:
                recipe = current.db(current.db.recipes.original_url == url).select().first()
                if recipe is None:
                    queued = True
                    recipe_id = current.db.recipes.insert(recipe_title=None,
                                                          original_url=url,
                                                          forked_from=None,
                                                          author=None,
                                                          publisher=None,
                                                          publication_date=None,
                                                          is_skeleton=True,
                                                          url=None)
                    RecipeInsertionManager.queued_urls[url] = recipe_id
                    if add_to_insertion_queue:
                        self.linked_recipes.append((recipe_id, url))
                    self.num_queued_recipes += 1
                else:
                    recipe_id = recipe.id
            else:
                recipe_id = RecipeInsertionManager.queued_urls[url]

            RecipeInsertionManager.url_queuing_lock.release()
        else:
            recipe_id = recipe.id

        # recipe_id should be assigned to not None in all blocks of if-else statements
        assert (recipe_id is not None)

        return queued, recipe_id

    @staticmethod
    def _create_on_site_url(recipe_title):
        """
        Creates a url for crowd cook using the title of the recipe and a few characters from the
        hash of the user's id.

        Parts taken from a StackOverflow question referencing the Django framework:
            http://stackoverflow.com/a/295466/1392894
        Args:
            recipe_title:

        Returns: The url for crowd cook

        """
        import unicodedata
        url_portion = unicodedata.normalize('NFKD', recipe_title).encode('ascii', 'ignore')
        url_portion = unicode(re.sub('[^\w\s-]', '', url_portion).strip().lower())
        url_portion = re.sub('[-\s]+', '-', url_portion)
        user_id_hash = hashlib.sha256(str(current.auth.user_id)).hexdigest()[:10]
        return "{url_portion}-{user_hash}".format(url_portion=re.sub(r"\s+", '-', url_portion), user_hash=user_id_hash)

    def _insert_recipe(self, recipe, recipe_id):
        """
        Insert a recipe
        Args:
            recipe: A Recipe object. It's base fields as defined in Recipe.__init__ will be used to insert the recipe

        Returns: None

        """
        recipe.parse_recipe()
        current.db.recipes.update_or_insert(current.db.recipes.id == recipe_id,
                                            recipe_title=recipe.recipe_title,
                                            original_url=recipe.url,
                                            forked_from=-1,
                                            author=recipe.author,
                                            publisher=recipe.publisher,
                                            publication_date=recipe.pub_date,
                                            is_skeleton=False,
                                            url=RecipeInsertionManager._create_on_site_url(recipe.recipe_title))

        for group_index, instruction_dict in enumerate(recipe.instructions):
            group_id = current.db.instruction_groups.insert(recipe_id=recipe_id,
                                                            instruction_group_index=group_index,
                                                            instruction_group_title=instruction_dict["group_title"])
            for instruction_index, instruction in enumerate(instruction_dict["instruction_steps"]):
                current.db.instructions.insert(instruction_group_id=group_id,
                                               instruction_index=instruction_index,
                                               instruction_text=instruction)

        for group_index, ingredient_dict in enumerate(recipe.ingredients):
            group_id = current.db.ingredient_groups.insert(recipe_id=recipe_id,
                                                           group_title=ingredient_dict["group_title"],
                                                           ingredient_group_index=group_index)
            for ingredient_index, ingredient in enumerate(ingredient_dict["ingredients"]):
                ingredient_name_id = current.db(
                    current.db.ingredient_names.ingredient_name == ingredient["name"]).select().first()
                if ingredient_name_id is not None:
                    ingredient_name_id = ingredient_name_id.id
                else:
                    ingredient_name_id = current.db.ingredient_names.insert(ingredient_name=ingredient["name"])

                ingredient_id = current.db.ingredients.insert(ingredient_group_id=group_id,
                                                              ingredient_name_id=ingredient_name_id,
                                                              amount=ingredient["quantity"],
                                                              units=ingredient["units"]
                                                              )
                if ingredient["links"] is not None:
                    for link_proxy, link_start, link_end in ingredient["links"]:
                        print link_proxy
                        _, linked_recipe_id = self.queue_url(link_proxy.url, True)

                        current.db.ingredient_links.insert(ingredient_id=ingredient_id,
                                                           linked_recipe_id=linked_recipe_id,
                                                           link_start=link_start,
                                                           link_end=link_end)

    def insert_from_url(self, url):
        """
        Try to insert a recipe from the url.
        Args:
            url: Recipe url

        Returns: The recipe id of the inserted or found recipe
        Raises: Raised NoKnownRecipeRetrieverExeption if the recipe cannot be matched to a valid
        class implementing RetrievedRecipe.
        """
        # If the recipe is already in the database just return the id
        queued, recipe_id = self.queue_url(url)
        if not queued:
            print "Found recipe ..."
            return recipe_id

        # Recipe has been queued by this process
        print "inserting recipe ... {0}".format(url)
        recipe = self._get_recipe(url)
        if recipe is None:
            raise NoKnownRecipeRetrieverException()

        self._insert_recipe(recipe, recipe_id)

        inserted_skeletons = list()
        for skeleton_id, skeleton_url in self.linked_recipes:
            print "inserting skeleton... {0}".format(skeleton_url)
            skeleton_recipe = self._get_recipe(skeleton_url)
            if skeleton_recipe is None:
                raise NoKnownRecipeRetrieverException
            self.insert_from_url(skeleton_url)
            self._insert_recipe(skeleton_recipe, skeleton_id)

            inserted_skeletons.append(skeleton_url)

        current.db.commit()

        RecipeInsertionManager.url_queuing_lock.acquire()
        RecipeInsertionManager.queued_urls.pop(url)
        for skeleton_url in inserted_skeletons:
            RecipeInsertionManager.queued_urls.pop(skeleton_url)
        RecipeInsertionManager.url_queuing_lock.release()

        # Add new items to solr index
        solr.query.delta_all()

        return recipe_id

    @staticmethod
    def insert_from_ractive_dict(rd):
        recipe_url = RecipeInsertionManager._create_on_site_url(rd["recipe_title"])
        recipe_id = current.db.recipes.insert(recipe_title=rd["recipe_title"],
                                              original_url="",
                                              forked_from=-1,
                                              author=rd["author"],
                                              publisher=rd["publisher"],
                                              publication_date=rd["pub_date"],
                                              is_skeleton=False,
                                              url=recipe_url)

        for group_index, instruction_dict in enumerate(rd["instruction_groups"]):
            group_id = current.db.instruction_groups.insert(recipe_id=recipe_id,
                                                            instruction_group_index=group_index,
                                                            instruction_group_title=instruction_dict["name"])
            for instruction_index, instruction in enumerate(instruction_dict["instructions"]):
                text = instruction["instruction_text"].strip()
                if text != "":
                    current.db.instructions.insert(instruction_group_id=group_id,
                                                   instruction_index=instruction_index,
                                                   instruction_text=text)

        for group_index, ingredient_dict in enumerate(rd["ingredient_groups"]):
            group_id = current.db.ingredient_groups.insert(recipe_id=recipe_id,
                                                           group_title=ingredient_dict["name"],
                                                           ingredient_group_index=group_index)
            for ingredient_index, ingredient in enumerate(ingredient_dict["ingredients"]):
                ingredient_name = ingredient["name"].strip()
                if ingredient_name != "":
                    ingredient_name_id = current.db(
                        current.db.ingredient_names.ingredient_name == ingredient_name).select().first()
                    if ingredient_name_id is not None:
                        ingredient_name_id = ingredient_name_id.id
                    else:
                        ingredient_name_id = current.db.ingredient_names.insert(ingredient_name=ingredient_name)

                    current.db.ingredients.insert(ingredient_group_id=group_id,
                                                  ingredient_name_id=ingredient_name_id,
                                                  amount=ingredient["amount"],
                                                  units=ingredient["units"]
                                                  )

        current.db.commit()

        # Add new item to solr index
        solr.query.delta_all()

        return recipe_url
