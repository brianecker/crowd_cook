import re
import urllib2
from abc import ABCMeta, abstractmethod, abstractproperty
from bs4 import BeautifulSoup
from urlparse import urljoin

from applications.crowd_cook.modules.recipe_handling.recipe import Recipe
from applications.crowd_cook.modules.recipe_handling.recipe_retrieval.link_proxy import LinkProxy
from .invalid_recipe_url_error import InvalidRecipeUrlError


class RetrievedRecipe(Recipe):
    __metaclass__ = ABCMeta

    def __init__(self, url):
        """
        :raises: URLError if the url is somehow invalid
        """
        # Define the Recipe basic fields using the super constructor
        super(RetrievedRecipe, self).__init__()

        # Validate url
        self.url = url
        self.validate_url()
        page = urllib2.urlopen(url).read()
        self.soup = BeautifulSoup(page, "lxml")

    """""""""""""""""""""""""""""""""""""""""""""""""""""
     Class methods
    """""""""""""""""""""""""""""""""""""""""""""""""""""

    """""""""""""""""""""""""""""""""""""""""""""""""""""
     Properties
    """""""""""""""""""""""""""""""""""""""""""""""""""""

    @abstractproperty
    def re_url(self):
        raise NotImplementedError()

    @abstractproperty
    def base_url(self):
        raise NotImplementedError()

    """""""""""""""""""""""""""""""""""""""""""""""""""""
     Methods
    """""""""""""""""""""""""""""""""""""""""""""""""""""

    @abstractmethod
    def parse_recipe(self):
        """
        Parse the recipe from the page.
        Fills the specified fields defined in __init__
        Returns:
        """
        raise NotImplementedError()

    def _process_links(self, referring_page, links):
        """
        Args:
            referring_page: The page containing the links. Used to convert relative urls to absolute urls.
            links: A list of anchors around recipe ingredients

        Returns: A list containing the corresponding link to the recipe on crowdcook or to the recipe
        on its original site if it is on a site for which parsing is not supported.

        """
        processed_links = list()
        print "trying to process links"
        for link in links:
            if not link.has_attr("href"):
                continue
            link_url = link["href"]
            if link_url.startswith("/"):
                link_url = urljoin(referring_page, link_url)
            print link_url
            processed_links.append(LinkProxy(link_url))
            print processed_links

        return processed_links

    def validate_url(self):
        """
        :raises: InvalidRecipeUrlError if the url is invalid for the derived class
        """
        match = re.match(self.re_url(), self.url)
        if match is None:
            raise InvalidRecipeUrlError()
