class LinkProxy:
    """
    Provides a proxy object for the classes deriving RetrievedRecipe to setup for linked
    ingredients, instructions, products, etc.
    """
    def __init__(self, url):
        self.url = url