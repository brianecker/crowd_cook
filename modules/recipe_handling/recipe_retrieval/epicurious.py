import re

from ingredient_parsing import find_all_groups, re_quantity
from retrieved_recipe import RetrievedRecipe


class Epicurious(RetrievedRecipe):
    re_url = re.compile("(http://)?www.epicurious.com/recipes/food/views/(.*)")

    def __init__(self, url):
        RetrievedRecipe.__init__(self, url)

    def re_url(self):
        return re.compile("(http://)?www.epicurious.com/recipes/food/views/(.*)")

    def base_url(self):
        return "http://www.epicurious.com/"

    def _parse_ingredient(self, ingredient):
        ingredient_text = ingredient.text
        links = ingredient.find_all("a")
        link_data = list()
        link_proxies = list()
        if links is not None and len(links) > 0:
            # We have links, possibly to new recipes, that we need to process
            link_proxies = self._process_links(self.url, links)

        amount = None
        units = None
        matches = find_all_groups(re_quantity, ingredient_text)
        if len(matches) == 1:
            if "amount" in matches[0]:
                amount = matches[0]["amount"]
            if "units" in matches[0]:
                units = matches[0]["units"]

        ingredient_text = re.sub(re_quantity, "", ingredient_text).strip()

        for link, link_proxy in zip(links, link_proxies):
            link_text = link.text.strip()
            link_start = ingredient_text.index(link_text)
            link_end = link_start + len(link_text) + 1
            link_data.append((link_proxy, link_start, link_end))

        return {"name": ingredient_text,
                "quantity": amount,
                "units": units,
                "links": link_data}

    def _parse_ingredient_group(self, ingredient_group):
        group_title = ingredient_group.find("strong")
        if group_title is not None:
            group_title = group_title.text.strip()
        ingredients = [self._parse_ingredient(ingredient)
                       for ingredient in ingredient_group.find_all("li", {"class": "ingredient"})]
        return {"group_title": group_title, "ingredients": ingredients}

    def _parse_instruction_group(self, instruction_group):
        group_title = instruction_group.find("strong")
        if group_title is not None:
            group_title = group_title.text.strip()
        steps = [step.text.strip() for step in instruction_group.find_all("li", {"class": "preparation-step"})]
        return {"group_title": group_title, "instruction_steps": steps}

    def parse_recipe(self):
        def text_or_none(el):
            return el.text.strip() if el is not None else None
        title_source = self.soup.find("div", {"class": "title-source"})
        self.recipe_title = title_source.find("h1", {"itemprop": "name"}).text.strip()
        self.author = text_or_none(title_source.find("span", {"itemprop": "author"}))
        self.publisher = text_or_none(title_source.find("span", {"itemprop": "publisher"}))
        self.pub_date = text_or_none(title_source.find("span", {"class": "pub-date"}))
        self.recipe_yield = text_or_none(self.soup.find("dd", {"class": "yield"}))
        self.ingredients = map(self._parse_ingredient_group,
                               self.soup.find("ol", {"class": "ingredient-groups"}))
        self.instructions = map(self._parse_instruction_group,
                                self.soup.find("ol", {"class": "preparation-groups"}).find_all("li", {
                                    "class": "preparation-group"}))
