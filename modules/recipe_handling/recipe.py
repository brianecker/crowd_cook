import json
from abc import ABCMeta
from gluon import current
from gluon.serializers import json
from ingredient_handling import *


class Recipe:
    __metaclass__ = ABCMeta

    SKELETON_RECIPE_URL = "insertion-in-progress"

    def __init__(self):
        """
        Constructor defining basic fields for a Recipe
        Returns:

        """
        self.recipe_title = None
        self.author = None
        self.publisher = None
        self.pub_date = None
        self.recipe_yield = None
        self.ingredients = list()
        self.instructions = list()

    """""""""""""""""""""""""""""""""""""""""""""""""""""
     Class methods
    """""""""""""""""""""""""""""""""""""""""""""""""""""

    @staticmethod
    def _insertion_in_progress_link(recipe_id):
        return "{base}/{id}".format(base=Recipe.SKELETON_RECIPE_URL, id=recipe_id)

    @staticmethod
    def get_recipe_url_from_id(recipe_id):
        db = current.db
        recipe = db(db.recipes.id == recipe_id).select().first()
        if recipe.is_skeleton:
            return Recipe._insertion_in_progress_link(recipe_id)

        return recipe.url if recipe is not None else None

    @staticmethod
    def get_recipe_id_from_url(recipe_url):
        db = current.db
        recipe = db(db.recipes.url == recipe_url).select().first()
        return recipe.id if recipe is not None else None

    @staticmethod
    def _get_forked(forked_id):
        db = current.db
        if forked_id != 0:
            forked_from = db(db.recipes.id == forked_id).select().first()
            if forked_from is not None:
                return dict(recipe_title=forked_from.recipe_title,
                            url=URL('default', 'recipe', args=[forked_from.url]))

        return -1

    @staticmethod
    def load_from_db(recipe_id):
        db = current.db
        if db(db.recipes.id == recipe_id).select().first() is not None:
            db_recipe = db(db.recipes.id == recipe_id).select().first()
            favorites = db_recipe.recipe_favorites.select()
            recipe = {
                "id": db_recipe.id,
                "recipe_title": db_recipe.recipe_title,
                "original_url": db_recipe.original_url,
                "forked_from": Recipe._get_forked(db_recipe.forked_from),
                "author": db_recipe.author,
                "publisher": db_recipe.publisher,
                "publication_date": db_recipe.publication_date,
                "is_skeleton": db_recipe.is_skeleton,
                "ingredient_groups": list(),
                "instruction_groups": list(),
                "favorites": favorites,
                "user_favorited": current.auth.user_id in [fav.user_id for fav in favorites] if current.auth.is_logged_in() else False
            }
            for ingredient_group in db_recipe.ingredient_groups.select():
                grp = {
                    "index": ingredient_group.ingredient_group_index,
                    "group_title": ingredient_group.group_title,
                    "ingredients": list()
                }

                ingrs = [ingr for ingr in ingredient_group.ingredients.select()]
                names = {name.id: name.ingredient_name for name in
                         db(db.ingredient_names.id.belongs(set(ingr.ingredient_name_id for ingr in ingrs))).select()}
                assert(len(ingrs) == len(names))
                for ingr in ingrs:
                    ingredient = {
                        "id": ingr.id,
                        "ingredient_name": names[ingr.ingredient_name_id],
                        "amount": ingr.amount,
                        "units": ingr.units,
                        "links_to": json(get_ingredient_links(ingr)),
                    }

                    grp["ingredients"].append(ingredient)

                recipe["ingredient_groups"].append(grp)

            for instruction_group in db_recipe.instruction_groups.select():
                grp = {
                    "group_title": instruction_group.instruction_group_title,
                    "index": instruction_group.instruction_group_index,
                    "instructions": list()
                }

                for inst in instruction_group.instructions.select():
                    instruction = {
                        "index": inst.instruction_index,
                        "instruction_text": inst.instruction_text
                    }

                    grp["instructions"].append(inst)

                recipe["instruction_groups"].append(grp)

            return recipe
