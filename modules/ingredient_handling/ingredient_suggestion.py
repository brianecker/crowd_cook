from applications.crowd_cook.modules.taste_profiling.taste_profiling import suggestion_score
from gluon import current


def get_ingredient_suggestions(ingredient_id, logged_in_user_id):
    db = current.db
    suggestions = db(db.ingredient_suggestions.ingredient_id == ingredient_id).select()
    results = list()
    for sugg in suggestions:
        suggesting_user = db(db.auth_user.id == sugg.suggesting_user).select().first()
        votes_up = sugg.ingredient_suggestion_votes_up.select()
        votes_down = sugg.ingredient_suggestion_votes_down.select()
        suggestion = {
            "suggestion_id": sugg.id,
            "ingredient_id": sugg.ingredient_id,
            "suggested_quantity": sugg.suggested_quantity,
            "votes_up": len(votes_up),
            "votes_down": len(votes_down),
            "suggested_by_me": False if (suggesting_user.id != logged_in_user_id) else True,
            "vote_direction": None,
            "suggestion_score": suggestion_score(votes_up, votes_down) if current.auth.is_logged_in() else 0.0,
        }

        print "suggestion_score: {0}".format(suggestion["suggestion_score"])

        def get_vote(user_id, suggestion_id, vote_table):
            return db(
                (vote_table.ingredient_suggestion_id == suggestion_id) &
                (vote_table.voting_user == user_id)
            ).count()

        if logged_in_user_id is not None:
            # Figure out if the logged in user has voted on this suggestion
            vote_up = get_vote(logged_in_user_id, sugg.id, db.ingredient_suggestion_votes_up)
            vote_down = get_vote(logged_in_user_id, sugg.id, db.ingredient_suggestion_votes_down)
            if vote_up != 0:
                suggestion["vote_direction"] = "up"
            elif vote_down != 0:
                suggestion["vote_direction"] = "down"

            suggestion["current_vote_direction"] = suggestion["vote_direction"];

        results.append(suggestion)

    return results


def vote(user_id, ingredient_suggestion_id, direction):
    db = current.db
    suggestion = db(db.ingredient_suggestions.id == ingredient_suggestion_id).select().first()
    if suggestion.suggesting_user == user_id:
        return dict()
    if direction == "up":
        insert_table = db.ingredient_suggestion_votes_up
    elif direction == "down":
        insert_table = db.ingredient_suggestion_votes_down
    else:
        return dict()

    def clear_votes(table):
        db(
            (table.voting_user == user_id) &
            (table.ingredient_suggestion_id == ingredient_suggestion_id)
        ).delete()

    # clear votes from this user on this suggestion
    clear_votes(db.ingredient_suggestion_votes_up)
    clear_votes(db.ingredient_suggestion_votes_down)

    # Reinsert new vote
    insert_table.insert(
        ingredient_suggestion_id=ingredient_suggestion_id,
        voting_user=user_id
    )

    return dict()


def insert_suggestion(ingredient_id, suggested_quantity, suggesting_user_id):
    """
    Checks if the suggestion has already been suggested, and if it has not, then
    insert it.
    Args:
        ingredient_id:
        suggested_quantity:
        suggesting_user_id:

    Returns: A representative dictionary of the inserted suggestion, or None if the suggestion already exists
    """
    db = current.db
    try:
        new_suggestion = {
            "ingredient_id": int(ingredient_id),
            "suggested_quantity": float(suggested_quantity),
            "suggesting_user": suggesting_user_id,
        }
    except ValueError:
        return dict()
    if db((db.ingredient_suggestions.ingredient_id == ingredient_id) &
                  (db.ingredient_suggestions.suggesting_user == current.auth.user_id)).select().first() is not None:
        return dict(error="You already have a suggestion for this ingredient")
    elif db((db.ingredient_suggestions.ingredient_id == ingredient_id) &
                  (db.ingredient_suggestions.suggested_quantity == suggested_quantity)).select().first() is not None:
        return dict(error="Suggestion exists")
    else:
        result_ids = db.ingredient_suggestions.bulk_insert([new_suggestion])
        result_id = result_ids[0]
        return dict(
            suggestion_id=result_id,
            votes_up=0,
            votes_down=0,
            suggested_by_me=True,
            suggested_quantity=suggested_quantity,
            suggestion_score=0
        )



