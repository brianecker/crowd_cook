from gluon import current


def get_ingredient_links(ingr):
    """
    Gets ingredient text and url for linked ingredients
    Args:
        ingr: Ingredient object
    Returns: list of dictionaries where each dictionary has "link_text" and "url" keys

    """
    db = current.db
    db(db.ingredient_links.ingredient_id == ingr.id).select()
    links = db(db.ingredient_links.ingredient_id == ingr.id).select()
    recipes = [db(db.recipes.id == link.linked_recipe_id).select().first() for link in links]

    # Figure out what sections of text overlap
    results = {"links": list()}
    for link, recipe in zip(links, recipes):
        results["links"].append({"url": recipe.url, "start": link.link_start, "end": link.link_end})
    results["links"].sort(key=lambda x: x["start"])

    return results
