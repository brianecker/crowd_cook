from applications.crowd_cook.modules.user_handling.user_handling import get_user
from gluon import current

TASTE_PROFILE_ITEMS = ["spicy", "sweet", "salty", "sour"]
MU = 0.02


def _resemblance_score(other_user):
    user = current.auth.user
    # for taste in TASTE_PROFILE_ITEMS:
    #     mytaste = user.__getattr__(taste)
    #     othertaste = other_user.__getattr__(taste)
    #     print "user taste: {0}, other taste: {1}, abs difference: {2}".format(mytaste, othertaste,
    #                                                                           abs(mytaste - othertaste))

    return min((
        1 - ((1.0 / len(TASTE_PROFILE_ITEMS)) * sum([abs(user.__getattr__(taste) - other_user.__getattr__(taste))
                                                    for taste in TASTE_PROFILE_ITEMS])) + MU),
        1.0)


def suggestion_score(votes_up, votes_down):
    def get_resemblance_scores(votes):
        if len(votes) == 0:
            return 0.0
        else:
            return float(sum([_resemblance_score(get_user(vote.voting_user)) for vote in votes])) / len(votes)

    # up_res = get_resemblance_scores(votes_up)
    # down_res = get_resemblance_scores(votes_down)
    # print "up_res: {0}".format(up_res)
    # print "down_res: {0}".format(down_res)

    return round((get_resemblance_scores(votes_up) - get_resemblance_scores(votes_down)) * 100)
