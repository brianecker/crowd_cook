import math
from datetime import datetime

from gluon import current


class PopularRecipeRetriever:
    """
    A caching class to control how often popular recipes are updated.
    Recipes are updated on request after UPDATE_TIME seconds, or they are
    retrieved from the cache.
    """
    # Update at a maximum every 600 seconds
    UPDATE_TIME = 1
    MAX_RECIPES = 100
    # List of current popular recipes
    popular_recipes = list()
    last_updated = None

    def __init__(self):
        pass

    @staticmethod
    def _rebuild_popular_recipes(now):
        db = current.db
        rows = db(db.recipes.is_skeleton == False).select()

        def log_time_value(t, log_base=60):
            delta = (now - t).total_seconds()
            if delta < log_base:
                delta = log_base
            return 1.0 / math.log(delta, log_base)

        def log_time_decay(row):
            # Sum the log time difference if time is less than now and time exists on favorite row
            recipe_favorites = row.recipe_favorites.select()
            time_decay = sum([log_time_value(favorite.favorited_on)
                              for favorite in recipe_favorites
                              if favorite.favorited_on is not None and favorite.favorited_on <= now], 0.0)
            num_favorites = len(recipe_favorites)
            row.num_favorites = num_favorites
            return time_decay

        rows = rows.sort(lambda row: log_time_decay(row), reverse=True)
        for r in rows:
            print "{0}, score={1}".format(r.recipe_title, log_time_decay(r))

        PopularRecipeRetriever.last_updated = datetime.utcnow()
        PopularRecipeRetriever.popular_recipes = rows[:PopularRecipeRetriever.MAX_RECIPES]

    def get_popular_recipes(self, n):
        """
        Gets some popular recipes
        Args:
            n: How many recipes to get

        Returns: A list of n popular recipes as database rows
        """
        now = datetime.utcnow()

        if (PopularRecipeRetriever.last_updated is None or (
            now - PopularRecipeRetriever.last_updated).total_seconds() > PopularRecipeRetriever.UPDATE_TIME):
            # Now get the lock and check if we still need to update

            if self.last_updated is None or (now - self.last_updated).total_seconds() > self.UPDATE_TIME:
                print("rebuilding popular recipes")
                self._rebuild_popular_recipes(now)

        return self.popular_recipes[:n]
