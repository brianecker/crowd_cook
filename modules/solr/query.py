import json
from urllib import quote_plus
from urllib2 import urlopen


RECIPES_BASE_URL = "http://127.0.1.1:8983/solr/crowd_cook"
INGREDIENTS_BASE_URL = "http://127.0.1.1:8983/solr/ingredient_lookup"


def _query(search_query, core_url):
    encoded = quote_plus(search_query)
    connection = urlopen("{core_url}/select?q={query}&wt=json".format(core_url=core_url, query=encoded))
    return json.load(connection)


def _full_update(core_url):
    connection = urlopen("{core_url}/dataimport?command=full-import&clean=true".format(core_url=core_url))
    return connection.getcode()


def _delta_update(core_url):
    """
    Use a trick for more efficient (smaller number of total queries) delta updating.
    See: http://wiki.apache.org/solr/DataImportHandlerDeltaQueryViaFullImport

    Returns: error code

    """
    connection = urlopen("{core_url}/dataimport?command=full-import&clean=false".format(core_url=core_url))
    return connection.getcode()


def recipes_query(search_query):
    return _query(search_query, RECIPES_BASE_URL)


def recipes_full_update():
    return _full_update(RECIPES_BASE_URL)


def recipes_delta_update():
    return _delta_update(RECIPES_BASE_URL)


def ingredients_query(search_query):
    return _query(search_query, INGREDIENTS_BASE_URL)


def ingredients_full_update():
    return _full_update(INGREDIENTS_BASE_URL)


def ingredients_delta_update():
    return _delta_update(INGREDIENTS_BASE_URL)


def get_n_suggested_ingredients(query):
    pass


def delta_all():
    return [recipes_delta_update(), ingredients_delta_update()]
